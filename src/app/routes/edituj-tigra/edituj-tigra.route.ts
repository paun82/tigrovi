import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TigroviModel } from '../../models/tigrovi.model';
import { TigarComponent } from '../tigar/tigar.route';

@Component({
  selector: 'edituj-tigra',
  templateUrl: './edituj-tigra.route.html'
})
export class EditujTigraComponent implements OnInit {

  editovaniTigar = {};

  constructor(private route:ActivatedRoute,public model:TigroviModel) {
    this.route.parent.params.subscribe(
      ({id}) => {
        this.model.getTigraById(id,(tigar)=>{
          this.editovaniTigar = tigar;
        });
      }
    );
  }

  updejtujTigra(){
    this.model.updejtujTigra(this.editovaniTigar);
    
  }

  ngOnInit() {
  }

}
