import { Component, OnInit } from '@angular/core';
import { TigroviModel } from '../../models/tigrovi.model';

@Component({
  selector: 'novi-tigar',
  templateUrl: './novi-tigar.route.html',
})
export class NoviTigarComponent implements OnInit {

  noviTigar = {
    ime:null,
    kilaza:null
  }

  constructor(public model:TigroviModel) { }

  ngOnInit() {
  }

  dodajTigra() {
    console.log(this.noviTigar);
    if(this.noviTigar.ime && this.noviTigar.kilaza) {
      this.model.dodajTigra(this.noviTigar);
    }
   
  }

}
