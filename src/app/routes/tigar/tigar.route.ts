import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'tigar',
  templateUrl: './tigar.route.html',
})
export class TigarComponent implements OnInit {

  id;
  constructor(private router:Router, private route:ActivatedRoute) {
    this.route.params.subscribe(({id}) => {
      this.id = id;
    });
   }

  ngOnInit() {
  }

  goToPath(subpath){
    this.router.navigate(['/tigar', this.id, subpath]);
  }

}
