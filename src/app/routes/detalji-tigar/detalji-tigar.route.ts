import { Component, OnInit } from '@angular/core';
import { TigroviModel } from '../../models/tigrovi.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'detalji-tigar',
  templateUrl: './detalji-tigar.route.html',
})
export class DetaljiTigarComponent implements OnInit {

  tigar = {};
  constructor(private route:ActivatedRoute,public model:TigroviModel) {
    this.route.parent.params.subscribe(
      ({id}) => {
        this.model.getTigraById(id,(tigar)=>{
          this.tigar = tigar;
        });
      }
    );
  }

  ngOnInit() {
  }

}
