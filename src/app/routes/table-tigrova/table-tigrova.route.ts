import { Component, OnInit } from '@angular/core';
import { TigroviModel } from '../../models/tigrovi.model';
import { config } from '../../config/config';
import { Router } from '@angular/router';

@Component({
  selector: 'table-tigrova',
  templateUrl: './table-tigrova.route.html',
})
export class TableTigrovaComponent implements OnInit {

  searchString = "";
  conf = config.tigroviTableConfig;

  constructor(public model:TigroviModel, private router:Router) { }

  ngOnInit() {
  }

  obrisiTigra(id){
    this.model.obrisiTigra(id);
  }

  editujTigra(id){
    this.router.navigate(['/tigar',id,'edit']);
  }

}
