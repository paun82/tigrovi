import { Component, OnInit } from '@angular/core';
import { MenuModel } from '../../models/menu.model';

@Component({
  selector: 'amenu',
  templateUrl: './menu.html'
})
export class MenuComponent implements OnInit {

  // linkovi = [
  //   {putanja:"", naziv:"Kuca"},
  //   {putanja:"", naziv:"Tabela tigrova"},
  //   {putanja:"", naziv:"Dodaj tigra"}
  // ]

  constructor(public model:MenuModel) { }

  ngOnInit() {
  }

}
