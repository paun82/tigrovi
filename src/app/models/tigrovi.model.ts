import { Injectable } from '@angular/core';
import { TigroviService } from '../services/tigrovi.service';

@Injectable()
export class TigroviModel{

    tigrovi = [];

    constructor(private service:TigroviService){

        this.refreshujTigrove(() => {});


        // let posmatracSvihTigrova = this.service.getAll();
        // posmatracSvihTigrova.subscribe(
        //     (tigrovi)=>{
        //         this.tigrovi = tigrovi;
        //     }
        // );
    }

    refreshujTigrove(clbk) {
        this.service.getAll().subscribe(
            (tigrovi) => {
                this.tigrovi = tigrovi;
                clbk()
            }
        );
    }

    updejtujTigra(tigar){
        this.service.update(tigar).subscribe((updejtovaniTigar)=>{
            this.refreshujTigrove(()=>{})
        })
    }

    getTigraById(id, clbk){
        this.service.getById(id).subscribe(clbk);
    }

    getTigraByIdCallback(id, clbk){
        if(this.tigrovi.length < 1){
            this.refreshujTigrove(()=>{
                for (let i = 0; i < this.tigrovi.length; i++) {
                    if (id == this.tigrovi[i].id) {
                        clbk(this.tigrovi[i])
                    }
                }
            })
        }else{
            for (let i = 0; i < this.tigrovi.length; i++) {
                if (id == this.tigrovi[i].id) {
                    clbk(this.tigrovi[i])
                }
            }
        }
        
    }

    dodajTigra(tigar) {
        this.service.post(tigar).subscribe(
            (tigar) => {
                this.tigrovi.push(tigar);
            }
        )
    }

    dajMiIndexTigraPoIdju(id,clbk) {
        for (let i = 0; i < this.tigrovi.length; i++) {
            if (id == this.tigrovi[i].id) {
                clbk(i)
            }
        }
    }

    obrisiTigra(id) {
        this.service.delete(id).subscribe(() => {
           this.dajMiIndexTigraPoIdju(id,(index)=>{
                this.tigrovi.splice(index,1);
            });
           
        });
    }

    

}