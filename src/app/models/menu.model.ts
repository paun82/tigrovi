import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MenuModel {

  linkovi = [
    {putanja:"", naziv:"Kuca"},
    {putanja:"/dashboard", naziv:"Tabela tigrova"},
    {putanja:"/tigar/novi", naziv:"Dodaj tigra"}
  ]
  constructor(private router:Router) { }

  goToPath(path) {
    let pathArray = [];
    pathArray.push(path);
    this.router.navigate(pathArray);
  }
}
