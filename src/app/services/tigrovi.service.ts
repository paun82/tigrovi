import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { config } from '../config/config';
import "rxjs/add/operator/map";

@Injectable()
export class TigroviService {

    constructor(private http: Http) { }

    getAll() {
        return this.http
            .get(config.apiUrl + '/tigrovi')
            .map((response) => { return response.json() });
    }

    getById(id) {
        return this.http
            .get(config.apiUrl + '/tigrovi/'+ id)
            .map((response) => { return response.json() });
    }

    post(tigar) {
        return this.http
            .post(config.apiUrl + '/tigrovi', tigar)
            .map((response) => { return response.json() });
    }

    update(tigar) {
        return this.http
            .patch(config.apiUrl + '/tigrovi/'+ tigar.id, tigar)
            .map((response) => { return response.json() });
    }

    delete(id) {
        return this.http
            .delete(config.apiUrl + '/tigrovi/'+ id)
            .map((response) => { return response.json() });
    }

    

}