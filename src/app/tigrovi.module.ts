import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { HttpModule } from '@angular/http'
import { FormsModule } from '@angular/forms';


import { TigroviComponent } from './tigrovi.component';
import { HeaderComponent } from './components/header/header';
import { MenuComponent } from './components/menu/menu';
import { SidebarComponent } from './components/sidebar/sidebar';
import { FooterComponent } from './components/footer/footer';
import { HomeComponent } from './routes/home/home.route';
import { TableTigrovaComponent } from './routes/table-tigrova/table-tigrova.route';
import { NoviTigarComponent } from './routes/novi-tigar/novi-tigar.route';
import { TigarComponent } from './routes/tigar/tigar.route';
import { EditujTigraComponent } from './routes/edituj-tigra/edituj-tigra.route';
import { MenuModel } from './models/menu.model';
import { DetaljiTigarComponent } from './routes/detalji-tigar/detalji-tigar.route';
import { MenuLinkComponent } from './components/menu-link/menu-link.component';

import { TigroviService } from './services/tigrovi.service';
import { TigroviModel } from './models/tigrovi.model';
import { FilterPipe } from './pipes/filter.pipe';
// import {HighlightDirective} from './directives/highlight'

let routes:Routes = [
  {path:'', component: HomeComponent},
  {path:'dashboard', component: TableTigrovaComponent},
  {path:'tigar/novi', component: NoviTigarComponent},
  {
    path:'tigar/:id', component: TigarComponent, children: [
      {path:'edit', component: EditujTigraComponent},
      {path:'info', component: DetaljiTigarComponent}
    ]
  },
  
]

@NgModule({
  declarations: [
    TigroviComponent,
    HeaderComponent,
    MenuComponent,
    SidebarComponent,
    FooterComponent,
    HomeComponent,
    TableTigrovaComponent,
    NoviTigarComponent,
    TigarComponent,
    EditujTigraComponent,
    DetaljiTigarComponent,
    MenuLinkComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpModule,
    FormsModule
  ],
  providers: [
    MenuModel,
    TigroviService,
    TigroviModel

  ],
  bootstrap: [TigroviComponent]
})
export class TigroviModule { }
